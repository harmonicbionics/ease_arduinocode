/*
  Motors Control

  This sketch does the following:
  1) Gets update for the speed from another Arduino connected to the EASE master.
  2) Depending on the motor chosen, it does the following
      2.1) If the motor chosen is a DC Motor, the motor is set to run at the set speed.
           The current speed of the motor is printed on the LCD.
      2.2) If the motor chosen is a Servo Motor, the motor is set at the desired position.
           The current position of the motor is printed on the LCD.
      2.3) If the motor chosen is a Stepper Motor, the motor is set to run at the set speed.          

  created 18 Dec 2019
  by Harmonic Binonics Inc. (https://www.harmonicbionics.com/). 
  
*/

/*
    Buton Encoding Relation
    -------------------------------------------
    |    Button on LCD     |    Encoded Value  |
    -------------------------------------------
    |         Left         |          1        |
    |          Up          |          2        |
    |         Down         |          3        |
    |         Right        |          4        |
    |        Select        |          5        |
    --------------------------------------------
*/
 
/*
    Motor Select Options
    |--------------------------------------------------------|
    |   Motor Select Integer Value   |    Motor Controlled   |
    |--------------------------------------------------------|
    |               0                |       Servo Motor     |
    |               1                |         DC  Motor     |
    |               2                |     Stepper Motor     |
    |--------------------------------------------------------|
*/

#include <Wire.h>
#include <Adafruit_MotorShield.h>   // Include the Adafruit Motor Shield Library

#include <Servo.h>   // Include the Arduino Servo Library
#include <Esmacatshield.h>      //Include the Esmacat Arduino Library

// Define the Pin Numbers as Macros
# define servo_control_pin 9   // Servo is connected to Servo port #1 on the Motor Shield
# define Serial_baud_rate 9600
# define ARDUINO_UNO_SLAVE_SELECT 10      // The chip selector pin for Arduino Uno is 10 

// Create objects for various motor classes and EASE
Esmacatshield ease_with_motor(ARDUINO_UNO_SLAVE_SELECT);      // Create a slave object and specify the Chip Selector Pin

// Create the motor shield object with the default I2C address
Adafruit_MotorShield AFMS = Adafruit_MotorShield(); 

// Create an object for Stepper Motor
Adafruit_StepperMotor *stepper_motor = AFMS.getStepper(200, 2);   // Connect a stepper motor with 200 steps per revolution
                                                                  // (1.8 degree) to motor port #2 (M3 and M4)
// Create an object for DC Motor
Adafruit_DCMotor *dc_motor = AFMS.getMotor(2);   // Connect a DC motor to motor port #2 (M2)

// Create an object for the Servo Motor Class
Servo servo_2;   // Servo is connected to Servo port #2 on the Motor Shield so the name servo_2 is used

// Required variables Initialization
int motor_select = 0;   // Variable to choose which motor to control
int servo_angle = 0;   // Variable to store the input angle
int stepper_speed = 0;   // Variable to store stepper motor speed
int dc_speed = 0;   // Variable to store dc motor speed
int ease_registers[8];      // EASE 8 registers

void setup() 
{
  Serial.begin(9600);           // set up Serial library at 9600 bps

  AFMS.begin();  // create with the default frequency 1.6KHz
  
  stepper_motor->setSpeed(10);  // 10 rpm 
  
  dc_motor->setSpeed(0);   // Set the initial Speed of DC Motor to 0
  // dc_motor->run(FORWARD);  

  servo_2.attach(servo_control_pin);   // Servo 1 on Adafruit Motor Shield is connected
                                       // to Pin 10 of the Arduino board
  ease_with_motor.start_spi();      // Start SPI for EASE
}

void loop() 
{
  ease_with_motor.get_ecat_registers(ease_registers);

  motor_select = ease_registers[0];   // Motor to control
  servo_angle = ease_registers[1];   // Actual Servo position
  dc_speed = ease_registers[2];   // Actual dc motor speed
  stepper_speed = ease_registers[3];   // Actual Stepper Motor Speed
  Serial.println(motor_select);
  if(motor_select == 0)
  {
    servo_2.write(servo_angle);
    Serial.print("Servo Angle ");
    Serial.println(servo_angle);
  }
  
  if(motor_select == 1)
  {
    if(dc_speed > 0)
    {
      dc_motor->setSpeed(dc_speed);
      dc_motor->run(FORWARD);
    }
    else
    {
      dc_motor->setSpeed(dc_speed);
      dc_motor->run(BACKWARD);
    }
    Serial.print("DC Speed");
    Serial.println(dc_speed);
  }

  if(motor_select == 2)
  {
    if(stepper_speed > 0)
    {
      stepper_motor->step(stepper_speed,FORWARD,SINGLE);
    }

    else
    {
      stepper_motor->step(stepper_speed,BACKWARD,SINGLE);
    }
    Serial.print("Stepper Speed");
    Serial.println(stepper_speed);
   }
}
