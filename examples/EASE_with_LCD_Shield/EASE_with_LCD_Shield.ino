/*
  Liquid Crystal & Esmacat Arduino Library - Printing the Latest Button Pressed

  This sketch does the following:
  1) Gets the analog value of the button pressed on the LCD Shield and 
     sends it to the EtherCAT master using EASE (EtherCAT Arduino Shield by Esmacat).
  2) Based on the Input from the master, prints the corresponding message on the LCD.

  created 15 Dec 2019
  by Harmonic Bionics Inc. (https://www.harmonicbionics.com/).
     
  
 */


/*
    PIN CONFIGURATION
    |-----------------------------------------|
    |  LCD PIN Number  |   Arduino PIN Number |
    |-----------------------------------------|
    |       RS         |      Digital Pin 8   |
    |     ENABLE       |      Digital Pin 9   |
    |       D4         |      Digital Pin 4   |
    |       D5         |      Digital Pin 5   |
    |       D6         |      Digital Pin 6   |
    |       D7         |      Digital Pin 7   |
    |     Buttons      |      Analog Pin A0   |
    |-----------------------------------------|
    
 */

/*
  Analog Input Values for the Push Buttons
  |------------------------------------------------|
  |    Push Button     |          A0 value         |
  |------------------------------------------------|
  |       SELECT       |   val >= 500 & val <= 750 |
  |        LEFT        |   val >= 300 & val < 500  |
  |         UP         |   val >= 50  & val < 150  |
  |        DOWN        |   val >= 150 & val < 300  |
  |        RIGHT       |   val >= 0   & val < 50   |
  |------------------------------------------------|
*/

#include <LiquidCrystal.h>      // Include LCD Arduino Library
#include <Esmacatshield.h>      //Include the Esmacat Arduino Library

#define ARDUINO_UNO_SLAVE_SELECT 10      // The chip selector pin for Arduino Uno is 10 

LiquidCrystal lcd_display(8, 9, 4, 5, 6, 7);      // Create an object for the Library class
Esmacatshield ease_1(ARDUINO_UNO_SLAVE_SELECT);      // Create a slave object and specify the Chip Selector Pin
                                

int ease_registers[8];      // EASE 8 registers

int analog_value;      // Initialise analog input value variable
String data;      // Initialize the data variable
int button_pressed;      // Encoded value for the buttons

/*
    -------------------------------------------
    |    Button on LCD     |    Encoded Value  |
    -------------------------------------------
    |         Left         |          1        |
    |          Up          |          2        |
    |         Down         |          3        |
    |         Right        |          4        |
    |        Select        |          5        |
    --------------------------------------------
 */

void setup() {
  
  lcd_display.begin(16,2);      //Initialise the number of (coloumns, rows) in the LCD
  
  lcd_display.print("Hello World ...");      // Print a message onto the LCD Display

  ease_1.start_spi();      // Start SPI for EASE
  
  Serial.begin(9600);      // Initialise the Serial Communication with the specified Baud Rate
}

void loop() {

  // Code to print the button pressed on the LCD to the Serial Monitor
  analog_value = analogRead(A0);
  
  ease_1.write_reg_value(0,analog_value);      //Write register data (register,value, led_on)
  
  ease_1.get_ecat_registers(ease_registers);
  button_pressed = ease_registers[1];      // Receive the encode the value for register 
  
  
  if (button_pressed == 1)
  {
    lcd_display.setCursor(0,1);      // set the LCD cursor position (Coloumn number,Row number)
    lcd_display.print("L"); 
    Serial.print(ease_registers[2]);
    Serial.println("\t Left Button Pressed");
  }
  
  if (button_pressed == 2)
  {
    lcd_display.setCursor(0,1);
    lcd_display.print("U"); 
    Serial.print(ease_registers[2]);
    Serial.println("\t Up Button Pressed");
  }

  if (button_pressed == 3)
  {
    lcd_display.setCursor(0,1);
    lcd_display.print("D"); 
    Serial.print(ease_registers[2]);
    Serial.println("\t Down Button Pressed");
  }
  
  if (button_pressed == 4)
  {
    lcd_display.setCursor(0,1);      
    lcd_display.print("R");
    Serial.print(ease_registers[2]); 
    Serial.println("\t Right Button Pressed");
  }

  if (button_pressed == 5)
  {
    lcd_display.setCursor(0,1);
    lcd_display.print("S"); 
    Serial.print(ease_registers[2]);
    Serial.println("\t Select Button Pressed");
  }
}
