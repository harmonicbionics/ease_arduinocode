/*
  Motors Control

  This sketch does the following:
  1) Gets user input for choice of motor from another Arduino connected to the EASE master.
  2) Depending on the motor chosen, it does the following
      2.1) If the motor chosen is a DC Motor, using the up and down buttons on the second Arduino
           the speed can be controlled.
           The current speed of the motor is printed on the LCD.
      2.2) If the motor chosen is a Servo Motor, using the up and down buttons on the second Arduino
           the angle can be controlled.
           The current position of the motor is printed on the LCD.
      2.3) If the motor chosen is a Stepper Motor, using the up and down buttons on the second Arduino
           the speed can be controlled.
           The current speed of the motor is printed on the LCD.            

  created 18 Dec 2019
  by Harmonic Binonics Inc. (https://www.harmonicbionics.com/).
  
*/

/*
    PIN CONFIGURATION of the LCD Shield Used
    |-----------------------------------------|
    |  LCD PIN Number  |   Arduino PIN Number |
    |-----------------------------------------|
    |       RS         |      Digital Pin 8   |
    |     ENABLE       |      Digital Pin 9   |
    |       D4         |      Digital Pin 4   |
    |       D5         |      Digital Pin 5   |
    |       D6         |      Digital Pin 6   |
    |       D7         |      Digital Pin 7   |
    |     Buttons      |      Analog Pin A0   |
    |-----------------------------------------|
    
*/

/*
    Analog Input Values for the Push Buttons
    |------------------------------------------------|
    |    Push Button     |          A0 value         |
    |------------------------------------------------|
    |       SELECT       |   val >= 500 & val <= 750 |
    |        LEFT        |   val >= 300 & val < 500  |
    |        DOWN        |   val >= 150 & val < 300  |
    |         UP         |   val >= 50  & val < 150  |
    |        RIGHT       |   val >= 0   & val < 50   |
    |------------------------------------------------|
*/

/*
    Buton Encoding Relation
    -------------------------------------------
    |    Button on LCD     |    Encoded Value  |
    -------------------------------------------
    |         Left         |          1        |
    |          Up          |          2        |
    |         Down         |          3        |
    |         Right        |          4        |
    |        Select        |          5        |
    --------------------------------------------
*/
 
/*
    Motor Select Options
    |--------------------------------------------------------|
    |   Motor Select Integer Value   |    Motor Controlled   |
    |--------------------------------------------------------|
    |               0                |       Servo Motor     |
    |               1                |         DC  Moror     |
    |               2                |     Stepper Motor     |
    |--------------------------------------------------------|
*/

#include <LiquidCrystal.h>      // Include LCD Arduino Library
#include <Esmacatshield.h>      //Include the Esmacat Arduino Library


// Define the Pin Numbers as Macros

# define Serial_baud_rate 9600
#define ARDUINO_UNO_SLAVE_SELECT 10      // The chip selector pin for Arduino Uno is 10 
# define RS_pin 8
# define Enable_pin 9
# define LCD_coloumns 16
# define LCD_rows 2

LiquidCrystal lcd_display(RS_pin, Enable_pin, 4, 5, 6, 7);      // Create an object for the Library class
Esmacatshield ease_with_lcd(ARDUINO_UNO_SLAVE_SELECT);      // Create a slave object and specify the Chip Selector Pin

int analog_value;      // Initialise analog input value variable
int servo_angle = 0;   // Variable to store the input angle
int stepper_speed = 0;   // Variable to store stepper motor speed
int dc_speed = 0;   // Variable to store dc motor speed
int motor_select = 0;   // Variable to choose the motor to be controlled
int prev_motor_select = 0;
int prev_servo_angle = 0;
int prev_dc_speed = 0;
int prev_stepper_speed = 0;

int ease_registers[8];      // EASE 8 registers


void setup() 
{
  lcd_display.begin(LCD_coloumns,LCD_rows);      //Initialise the number of (coloumns, rows) in the LCD
  
  lcd_display.print("Servo Motor");      // Print a message onto the LCD Display
                                         // Default is Servo Motor 
  Serial.begin(Serial_baud_rate);      // Initialise the Serial Communication with the specified Baud Rate
  
  ease_with_lcd.start_spi();      // Start SPI for EASE
}

void loop() 
{

  analog_value = analogRead(A0);
  Serial.println(analog_value);

  ease_with_lcd.write_reg_value(0,analog_value);      //Write register data (register,value, led_on)
  
  delay(50);
  
  ease_with_lcd.get_ecat_registers(ease_registers);
  servo_angle = ease_registers[1];   // Actual Servo position
  dc_speed = ease_registers[2];   // Actual dc motor speed
  stepper_speed = ease_registers[3];   // Actual Stepper Motor Speed
  motor_select = ease_registers[4];

  if(motor_select == 0)   // Servo Motor Selected
  {
    if(prev_motor_select != motor_select)
    {
      for(int i = 0; i < 16; i++)
      {
        lcd_display.setCursor(i,0);
        lcd_display.print(" ");      // Remove the previous characters (if any)
      }
    }
    lcd_display.setCursor(0,0);
    lcd_display.print("Servo Motor");
    if(prev_servo_angle != servo_angle)
    {
      for (int i = 0;i < 4;i++)
      {
        lcd_display.setCursor(i,1);
        lcd_display.print(" ");
      }
    }
    lcd_display.setCursor(0,1);
    lcd_display.print(servo_angle);
    lcd_display.setCursor(10,1);      // set the LCD cursor position (Coloumn number,Row number)
    lcd_display.print("angle"); 
  }
  
  if(motor_select == 1)   // DC Motor Selected
  {
    if(prev_motor_select != motor_select)
    {
      for(int i = 0; i < 16; i++)
      {
        lcd_display.setCursor(i,0);
        lcd_display.print(" ");      // Remove the previous characters (if any)
      }
    }
    
    lcd_display.setCursor(0,0);
    lcd_display.print("DC Motor");
    
    if(prev_dc_speed != dc_speed)
    {
      for (int i = 0;i < 4;i++)
      {
        lcd_display.setCursor(i,1);
        lcd_display.print(" ");
      }
    }
    
    lcd_display.setCursor(0,1);
    lcd_display.print(dc_speed);
    lcd_display.setCursor(10,1);      // set the LCD cursor position (Coloumn number,Row number)
    lcd_display.print("speed"); 
  }
  
  if(motor_select == 2)   // Stepper Motor Selected
  {
    if(prev_motor_select != motor_select)
    {
      for(int i = 0; i < 16; i++)
      {
        lcd_display.setCursor(i,0);
        lcd_display.print(" ");      // Remove the previous characters (if any)
      }
    }
    
    lcd_display.setCursor(0,0);
    lcd_display.print("Stepper Motor");
    
    if(prev_stepper_speed != stepper_speed)
    {
      for (int i = 0;i < 4;i++)
      {
        lcd_display.setCursor(i,1);
        lcd_display.print(" ");
      }
    }
    
    lcd_display.setCursor(0,1);
    lcd_display.print(stepper_speed);
    lcd_display.setCursor(10,1);      // set the LCD cursor position (Coloumn number,Row number)
    lcd_display.print("speed"); 
  }
  prev_motor_select = motor_select;
  prev_servo_angle = servo_angle;
  prev_dc_speed = dc_speed;
  prev_stepper_speed = stepper_speed;
}
